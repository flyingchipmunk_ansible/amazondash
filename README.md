# amazondash

Ansible role for installing and configuring amazon-dash pip module

## Requirements

### Control Machine Requirements
* Ansible >= 2.2

### Target Host Requirements
These pip modules are required on the target host and there is a task to install them already. They are listed here for completeness, there is no manual installation needed.

* subprocess32
* amazon-dash

## Role Variables

List of amazon dash devices and commands to trigger

    amazondash_devices:
      - hwaddr: '00:...'
        name: 'Reboot Button'
        cmd: "{{ amazondash_reboot_script }}"
        user: myusername
      - hwaddr: '00:...'
        name: 'My Other Button'
        cmd: /my/command
        user: myusername

Location/name of the commonly used reboot callback script

    amazondash_reboot_script: /usr/local/bin/reboot.sh

Location/name of the commonly used shutdown callback script

    amazondash_shutdown_script: /usr/local/bin/shutdown.sh

## Dependencies

None.

## Example Playbook

    - name: Setup amazondash button
      import_role:
        name: amazondash

## License

The MIT License (MIT)

Copyright (c) 2018 Matthew C. Veno

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

https://opensource.org/licenses/MIT

## Author Information

Matthew C. Veno
